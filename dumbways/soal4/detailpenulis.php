<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>

  <style>

      .post{
        margin-top:1.5rem;
        margin-bottom: 1.5rem;
        padding:35px;
        box-shadow: 2px 2px 7px #b2bec3;
      }

      table{
          margin:10px;
          margin-top:20px;
      }

      table th,td{
        width:500px;
        text-align: center;
      }

  </style>
  <body>

    <div class="container mt-5">

    <?php
        require_once('config.php');
        $id = $_GET['id_penulis'];
        
        $query = "SELECT * FROM book_tb INNER JOIN category_tb ON book_tb.category_id=category_tb.id_kategori INNER JOIN penulis_tb ON book_tb.writer_id=penulis_tb.id_penulis WHERE penulis_tb.id_penulis='$id'";
        $result = $link->query($query);
        $data = $result->fetch_assoc()

?>
       

        <a style="width:100%; " href="result.php"><button style="width:10%; margin-top:20px; position:absolute;" class="btn btn-primary"><-</button></a>
        <h2 style="text-align:center; margin-bottom:30px;">Informasi Detail Penulis</h2>
        <div style="margin:0px auto; width:500px;" class="post">

            <h3 style="text-align:center;margin-top:20px;"><?php echo $data['nama']?></h3>
            <table>

                <tr>
                    <th>Buku Diselesaikan </th>
                    <td>
                    <?php $query1 = "SELECT * FROM book_tb INNER JOIN penulis_tb WHERE book_tb.writer_id=penulis_tb.id_penulis"; 
                    $result1 = $link->query($query1);
                    ?>
                    <?php while($row=$result1->fetch_assoc()){?>
                    <?php echo $row['name'].' '.$data['publication_year'].",";?>
                    <?php } ?>
                    </td>
                </tr>

                <tr>
                    <th>Tanggal Lahir</th>
                    <td><?php echo $data['tgllahir'];?></td>
                </tr>

                <tr>
                    <th>Tempat Lahir</th>
                    <td><?php echo $data['tmptlahir'];?></td>
                </tr>

            </table>
            
            <a style="width:100%;" href="editpenulis.php?id_penulis=<?php echo $data['writer_id'];?>"><button style="width:100%;" class="btn btn-success">Edit</button></a>
            <a style="width:100%; " href="deletepenulis.php?id_penulis=<?php echo $data['id_penulis'];?>"><button style="width:100%; margin-top:20px;" class="btn btn-danger">Delete</button></a>
                    
        </div>





    </div>
   

    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>