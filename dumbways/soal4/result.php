<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hasil Data</title>
  </head>
  <body>
    
    <div class="container mt-5">
        <h1 style="text-align:center;">Data Buku</h1>
        <a href="insert.php"><button class="btn btn-primary">Tambahkan Data</button></a>
        <a href="tambahpenulis.php"><button class="btn btn-primary">Tambah Penulis</button></a>

    <?php
        require_once('config.php');

        $query = "SELECT * FROM book_tb INNER JOIN category_tb ON book_tb.category_id=category_tb.id_kategori INNER JOIN penulis_tb ON book_tb.writer_id=penulis_tb.id_penulis order by id desc";
        //$query  = "SELECT * FROM book_tb";
        $result = $link->query($query);

?>
        <div class="row">
        <?php while($data = $result->fetch_assoc()){
        ?>
            <div class="col-sm">
                <div class="card mt-5" style="width: 18rem;">
                <?php echo "<img style='width:150;' height='100' class='card-img-top' src='images/".$data['img']."' alt='Card image cap'>" ;?>
                    <div class="card-body ">
                        <h5 class="card-title"><?php echo $data['name']?></h5>
                        <div class="row m-1 mb-3">
                            <div class="col"><?php echo $data['publication_year']?></div>
                            <div class="col"><?php echo $data['nama']?></div>
                        </div>
                        <a href="show.php?id=<?php echo $data['id'];?>" style="width:100%;" class="btn btn-primary">View Details</a>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>

    </div>
   

    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>