<!doctype html>
<html lang="en">
  <?php  require_once('config.php');?>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
      <div class="container mt-5">
          <h1 style="text-align:center;">Form Isi Data</h1>
          <a href="result.php"><button class="btn btn-info">Tidak Jadi Update</button></a>
            <form class="mt-5" method="POST" action="upload.php" enctype="multipart/form-data">

            <div class="form-group">
                    <label for="exampleFormControlSelect1">Daftar Penulis</label>
                    <select name="writer_id" class="form-control">
                    <?php           

                    $query = "SELECT * FROM penulis_tb";
                    $result = $link->query($query);

                    while($data=$result->fetch_array()){
                    ?>
                    <option value="<?php echo $data['id_penulis'];?>"><?php echo $data['nama'];?></option>
                    <?php } ?>
                    
                    </select>
              </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Judul Buku</label>
                    <input name="nama" type="text" class="form-control" placeholder="Sherlock Holmes : Mengenang John Watson">
                </div>

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Tahun Publikasi</label>
                    <select name="publication_year" class="form-control" >
                    <option>2020</option>
                    <option>2019</option>
                    <option>2018</option>
                    <option>2017</option>
                    <option>2016</option>
                    <option>2015</option>
                    <option>2014</option>
                    <option>2013</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlSelect2">Category</label>
                    <select multiple class="form-control" name="category_id">
                    <?php
                    require_once('config.php');

                    $query = "SELECT * FROM category_tb";
                    $result = $link->query($query);

                    while($data = mysqli_fetch_assoc($result)){ 
                    ?>
                    <option value="<?php echo $data['id_kategori']?>"><?php echo $data['name_category']?></option>
                    <?php } ?>
                    </select>

                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Upload Gambar</label>
                    <br>
                    <input type="file" name="img">
                </div>

                <input type="submit" name="submit" style="position:relative; left:90%;" value="Upload" class="btn btn-primary">
            </form>

            
      </div>
        

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>