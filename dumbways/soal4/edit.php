<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
      <div class="container mt-5">

      <?php
        require_once('config.php');

        $id = $_GET['id'];
        
        $query = "SELECT * FROM book_tb inner join category_tb on book_tb.category_id=category_tb.id_kategori WHERE book_tb.id='$id'";
        $result = $link->query($query);
        
        $data = $result->fetch_assoc();
        $selected="";
       
        $tahun = '2020'; 
        if($tahun==$data['publication_year'])
        {$selected='selected';}
        
        ?>

          <h1 style="text-align:center;">Form Isi Data</h1>
          <a href="result.php"><button class="btn btn-info">Tidak Jadi Update</button></a>
            <form class="mt-5" method="POST" action="update.php" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Penulis Buku</label>
                    <select name="writer_id" id="" class="form-control">
                    <?php
                    require_once('config.php');

                    $query = "SELECT * FROM penulis_tb";
                    $result = $link->query($query);

                    while($row = mysqli_fetch_assoc($result)){ 
                        if($data['writer_id'] == $row['id_penulis']){
                            $selected='selected';
                        }else{
                            $selected='';
                        }
                    ?>
                    <option  <?php echo $selected; ?> value="<?php echo $row['id_penulis']?>"><?php echo $row['nama']?></option>
                    <?php } ?>

                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Judul Buku</label>
                    <input name="nama" type="text" value="<?php echo $data['name'];?>"class="form-control" placeholder="Sherlock Holmes : Mengenang John Watson">
                </div>

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Tahun Publikasi</label>
                    <input type="text" name="publication_year" disabled class="form-control" value="<?php echo $data['publication_year'];?>">
                    <br>
                    <select name="publication_year" class="form-control">
                    <?php 
                    for($i=2020;$i>2012;$i--){
                    ?>
                    <option <?php if($i==$data['publication_year']){echo $selected='selected';}else{$selected='';} ?>
                    ><?php echo $i;?></option>
                    <?php } ?>
                    
                    </select>
                </div>

                <div class="form-group">
                    <input type="text" name="id" value="<?php echo $data['id'];?>" hidden>
                    <label for="exampleFormControlSelect2">Category</label>
                    
                    <input type="text" name="category_id" class="form-control" value="<?php echo $data['category_id'];?>" hidden>
                    <br>
                    <select multiple class="form-control" name="category_id">
                    <?php
                    require_once('config.php');

                    $query = "SELECT * FROM category_tb";
                    $result = $link->query($query);

                    while($row = mysqli_fetch_assoc($result)){ 
                        if($data['category_id'] == $row['id_kategori']){
                            $selected='selected';
                        }else{
                            $selected='';
                        }
                    ?>
                    <option  <?php echo $selected; ?> value="<?php echo $row['id_kategori']?>"><?php echo $row['name_category']?></option>
                    <?php } ?>
                    </select>

                    

                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Upload Gambar</label>
                    <br>
                    <input type="file" name="img">
                </div>

                <input type="submit" name="submit" style="position:relative; left:90%;" value="Edit Data" class="btn btn-success">
                
            </form>
            <br><br>
      </div>
        

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>